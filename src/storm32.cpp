#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <string.h>

#include <string>
#include <iostream>
#include <cmath>

#include "../gimbal/src/checksum.h"
#include "./storm32.hpp"


#ifndef STORM32_PACKED
  #define STORM32_PACKED __attribute__((__packed__))
#endif
#define STORM32_HEADER_LENGTH 3
#define STORM32_CRC_LENGTH 2

// This byte is used at the beginning of a message as a flag byte.
#define STORM32_HEADER_FLAG_NORMAL 0xfa
// This is an alternative flag byte which supresses a response from the storm32.
#define STORM32_HEADER_FLAG_SUPRESS 0xf9

#define STORM32_CMD_SET_PITCH 0x0a
#define STORM32_CMD_SET_ROLL 0x0b
#define STORM32_CMD_SET_YAW 0x0c
#define STORM32_CMD_SET_MODE 0x0d
#define STORM32_CMD_SET_PITCH_ROLL_YAW 0x12

#define STORM32_MODE_OFF 0
#define STORM32_MODE_HOLDHOLDPAN 1
#define STORM32_MODE_HOLDHOLDHOLD 2
#define STORM32_MODE_PANPANPAN 3
#define STORM32_MODE_PANHOLDHOLD 4
#define STORM32_MODE_PANHOLDPAN 5
#define STORM32_MODE_HOLDPANPAN 6

#define LIVEDATA_TIMES 0x0002
#define LIVEDATA_IMU1GYRO 0x0004
#define LIVEDATA_IMU1ACC 0x0008
#define LIVEDATA_IMU1R 0x0010
#define LIVEDATA_IMU1ANGLES 0x0020
#define LIVEDATA_PIDCNTRL 0x0040
#define LIVEDATA_INPUTS 0x0080
#define LIVEDATA_IMU2ANGLES 0x0100
#define LIVEDATA_STATUS_DISPLAY 0x0200
#define LIVEDATA_STORM32LINK 0x0400
#define LIVEDATA_IMUACCCONFIDENCE 0x0800
#define LIVEDATA_ATTITUDE_RELATIVE 0x1000
#define LIVEDATA_STATUS_V2 0x2000
#define LIVEDATA_ENCODERANGLES 0x4000
#define LIVEDATA_IMUACCABS 0x8000

#define KEY_UP 65
#define KEY_DOWN 66
#define KEY_LEFT 68
#define KEY_RIGHT 67

namespace gimbal {

  Storm32::Storm32(std::string serial_port) {
    struct termios tty;
    // Open the serial port.

    fd = open("/dev/ttyAMA1", O_RDWR | O_NOCTTY | O_SYNC);

    if (fd < 0) {
        perror("Could not open serial device.");
    }

    if(tcgetattr(fd, &tty) != 0) {
        perror("Could not open serial device.");
    }

    // Clear parity bit, disabling parity (most common)
    tty.c_cflag &= ~PARENB;
    // Clear stop field, only one stop bit used in communication (most common)
    tty.c_cflag &= ~CSTOPB;
    // Clear all bits that set the data size
    tty.c_cflag &= ~CSIZE;
    // 8 bits per byte (most common)
    tty.c_cflag |= CS8;
    // Disable RTS/CTS hardware flow control (most common)
    tty.c_cflag &= ~CRTSCTS;
    // Turn on READ & ignore ctrl lines (CLOCAL = 1)
    tty.c_cflag |= CREAD | CLOCAL;

    tty.c_lflag &= ~ICANON;
    // Disable echo
    tty.c_lflag &= ~ECHO;
    // Disable erasure
    tty.c_lflag &= ~ECHOE;
    // Disable new-line echo
    tty.c_lflag &= ~ECHONL;
    // Disable interpretation of INTR, QUIT and SUSP
    tty.c_lflag &= ~ISIG;

    tty.c_lflag &= ~IEXTEN;
    // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);
    // Disable any special handling of received bytes
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

    // Prevent special interpretation of output bytes (e.g. newline chars)
    tty.c_oflag &= ~OPOST;
    // Prevent conversion of newline to carriage return/line feed
    tty.c_oflag &= ~ONLCR;

    // Wait for up to 1s (10 deciseconds), returning as soon as any data is
    // received.
    tty.c_cc[VTIME] = 10;
    tty.c_cc[VMIN] = 0;

    // Set in/out baud rate to be 115200
    cfsetispeed(&tty, B115200);
    cfsetospeed(&tty, B115200);

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
    printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
    }
  }

  Storm32::~Storm32() {
    close(fd);
  }

Result Storm32::setRoll(uint16_t roll) {

    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t roll;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = STORM32_HEADER_FLAG_NORMAL;
    buffer.field.len = sizeof(buffer) - STORM32_HEADER_LENGTH - STORM32_CRC_LENGTH;
    buffer.field.cmd = STORM32_CMD_SET_ROLL;
    buffer.field.roll = roll;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));
    
    return Result::OK;
  }

  Result Storm32::setPitch(uint16_t pitch) {
    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t pitch;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = STORM32_HEADER_FLAG_NORMAL;
    buffer.field.len = sizeof(buffer) - STORM32_HEADER_LENGTH - STORM32_CRC_LENGTH;
    buffer.field.cmd = STORM32_CMD_SET_PITCH;
    buffer.field.pitch = pitch;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));

    return Result::OK;
  }

  Result Storm32::setYaw(uint16_t yaw) {

    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t yaw;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = STORM32_HEADER_FLAG_NORMAL;
    buffer.field.len = sizeof(buffer) - STORM32_HEADER_LENGTH - STORM32_CRC_LENGTH;
    buffer.field.cmd = STORM32_CMD_SET_YAW;
    buffer.field.yaw = yaw;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));
    
    return Result::OK;
  }

  Result Storm32::restore_all_parameters() {
    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    buffer.field.stx = 0xFA;
    buffer.field.len = sizeof(buffer) - STORM32_HEADER_LENGTH - STORM32_CRC_LENGTH;
    buffer.field.cmd = 0x15;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    write(fd, buffer.array, sizeof(buffer));

    return Result::OK;
  }

  Result Storm32::setMode(int mode) {

    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint8_t mode;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = STORM32_HEADER_FLAG_NORMAL;
    buffer.field.len = sizeof(buffer) - STORM32_HEADER_LENGTH - STORM32_CRC_LENGTH;
    buffer.field.cmd = STORM32_CMD_SET_MODE;
    buffer.field.mode = mode;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));
    
    return Result::OK;
  }

  Result Storm32::setPwmOut(uint16_t pwm) {
    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t mode;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = 0xFA;
    buffer.field.len = 0x2;
    buffer.field.cmd = 0x13;
    buffer.field.mode = pwm;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));
    
    return Result::OK;
  }

  Result Storm32::setAngles(float roll, float pitch, float yaw) {
    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      float roll;
      float pitch;
      float yaw;
      uint8_t flags;
      uint8_t type;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = 0xFA;
    buffer.field.len = 0x0E;
    buffer.field.cmd = 0x11;
    buffer.field.roll = roll;
    buffer.field.pitch = pitch;
    buffer.field.yaw = yaw;
    buffer.field.flags = 0x03;
    buffer.field.type = 0;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));
    
    return Result::OK;

  }

  Result Storm32::getParameter(uint16_t paramNumber) {
    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t mode;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = 0xFA;
    buffer.field.len = 0x02;
    buffer.field.cmd = 0x03;
    buffer.field.mode = paramNumber;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));
    
    return Result::OK;
  }

  Result Storm32::getAngles(int fd, int16_t dataCode) {
    struct STORM32_PACKED Package {
      uint8_t stx;
      uint8_t len;
      uint8_t cmd;
      uint16_t mode;
      uint16_t crc;
    };

    union {
      Package field;
      uint8_t array[sizeof(Package)];
    } buffer;

    // Pack data into package struct.
    buffer.field.stx = 0xFA;
    buffer.field.len = 0x01;
    buffer.field.cmd = 0x05;
    buffer.field.mode = 0;
    buffer.field.crc = crc_x25_calculate(buffer.array, sizeof(buffer));

    // Write buffer to serial device.
    write(fd, buffer.array, sizeof(buffer));

    struct STORM32_PACKED PackageReceive {
      uint8_t srx;
      uint8_t len;
      //uint32_t time_boot_ms;
      uint8_t tmp3;
      uint8_t tmp4;
      uint64_t tmp1;
      uint64_t tmp2;
      int16_t pitch_deg;
      int16_t roll_deg;
      int16_t yaw_deg;
    };

    union {
      PackageReceive field;
      uint8_t array[sizeof(PackageReceive)];
    } buffer_read;

    int read_res = -15;

    int res = 0;
    int tmp;
    // while(read_res <= 0){
    //   read_res = read(fd, buffer_read.array, sizeof(buffer_read));
    //   if (read_res < buffer_read.array[1]) {
    //      while(tmp = read(fd, buffer_read.array+ read_res, sizeof(buffer_read)-read_res) != -1) {
    //      }
    //      res = read_res + tmp;
    //    }
    // }

    for(int i = 0; i< 42; i++) {
      printf("0x%x ", buffer_read.array[i]);
    }
    printf("\n");

    printf("%d\n", read_res);

    printf("Pitch: %hu ", buffer_read.field.pitch_deg);
    printf("Roll: %hu ", buffer_read.field.roll_deg);
    printf("Yaw: %hu ", buffer_read.field.yaw_deg);
    
    return Result::OK;
  }

}