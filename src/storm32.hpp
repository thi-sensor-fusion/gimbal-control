#pragma once

#include <string>

namespace gimbal {

  enum class Result : uint8_t {
    OK,
    NOT_CAPABLE,
  };

  class Storm32 {
    private:
      int fd;

    public:
      Storm32(std::string device);
      ~Storm32();

      Result setMovementSpeed(int32_t speed);

      Result setPitch(uint16_t pitch);

      Result setRoll(uint16_t roll);

      Result setYaw(uint16_t yaw);
      
      Result restore_all_parameters();
      
      Result setMode(int mode);

      Result setPwmOut(uint16_t pwm);

      Result setAngles(float roll, float pitch, float yaw);

      Result getParameter(uint16_t paramNumber);

      Result getAngles(int fd, int16_t dataCode);
  };

  enum class Mode : uint8_t {
    OFF,
    HOLD_HOLD_HOLD,
    HOLD_HOLD_PAN,
    HOLD_PAN_HOLD,
    HOLD_PAN_PAN,
    PAN_HOLD_HOLD,
    PAN_HOLD_PAN,
    PAN_PAN_HOLD,
    PAN_PAN_PAN,
    _size,
  };
}

