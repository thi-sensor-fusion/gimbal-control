#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <iostream>
#include <math.h>

#include "storm32.hpp"

int main() {
    gimbal::Storm32 gimbal = gimbal::Storm32("/dev/ttyAMA1");

    // char buf [50];

    // while(read(fd, buf, sizeof(buf)) == -1);
     
    // for(int i = 0; i< 15; i++) {
    //   printf("0x%x ", buf[i]);
    // }
    //usleep(1000000);

    //setAngles(fd, 20, 20, 20);

    float yaw = 0;
    float pitch = 0;
    float roll = 0;
    char keyboard;

    struct termios t;
    tcgetattr(STDIN_FILENO, &t);
    t.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &t);

    while(1) {
      std::cin >> keyboard;

      printf("%d\n", keyboard);

      switch(keyboard) {
        case 119: {
          // if (pitch < 44) {
            ++pitch;
          // } else {
          //   pitch = 44;
          // }
          break;
        }
        case 115: {
          // if (pitch > -23) {
            --pitch;
          // } else {
          //   pitch = -23;
          // }
          break;
        }
        case 100: {
          yaw = std::fmod(yaw - 1,360);
          break;
        }
        case 97: {
          yaw = std::fmod(yaw + 1,360);
          break;
        }
        default: goto cleanup; 
      }

      gimbal.setAngles(roll, pitch, yaw);
      printf("Roll: %f\n", roll);
      printf("Pitch: %f\n", pitch);
      printf("Yaw; %f\n\n", yaw);
    }

    // while(read(fd, buf, sizeof(buf)) == -1);
     
    // for(int i = 0; i< 15; i++) {
    //   printf("0x%x ", buf[i]);
    // }
    // printf("\n");

    // sleep(1);
    
    // // getParameter(fd,55);

    // getAngles(fd, 1);

    // printf("\n");

    // // getParameter(fd, 56);
    // // while(read(fd, buf, sizeof(buf)) == -1);
     
    // // for(int i = 0; i< 10; i++) {
    // //   printf("0x%x\n", buf[i]);
    // // }

    // // getParameter(fd, 57);
    
    // // while(read(fd, buf, sizeof(buf)) == -1);
     
    // // for(int i = 0; i< 10; i++) {
    // //   printf("0x%x\n", buf[i]);
    // // }

    // // usleep(1000000);


    // // setRoll(fd, 2000);

    // // usleep(1000000);

    // // setPitch(fd, 1000);
    cleanup:

    gimbal.~Storm32();

    return 0;
}